#include "helpers.hpp"
#include <algorithm>
#include <charconv>
#include <iomanip>
#include <iterator>
#include <ranges>
#include <string_view>
#include <vector>

struct Game {
  int id = -1;
  int red_cubes = -1;
  int green_cubes = -1;
  int blue_cubes = -1;

  void update_cubes(std::string_view type, int count) {
    switch (type[0]) {
    case 'r':
      red_cubes = red_cubes < count ? count : red_cubes;
      break;
    case 'g':
      green_cubes = green_cubes < count ? count : green_cubes;
      break;
    case 'b':
      blue_cubes = blue_cubes < count ? count : blue_cubes;
      break;
    default:
      std::cout << "Could not find proper cube type:" << type << "\n";
    }
  }

  void print() {
    std::cout << "GameID:" << id << "\t red: " << red_cubes
              << "\tgreen: " << green_cubes << "\tblue: " << blue_cubes << "\n";
  }

  int isGamePossible(int red, int green, int blue) {
    return (red_cubes <= red && green_cubes <= green && blue_cubes <= blue) ? id
                                                                            : 0;
  }

  long powerCubes() { return red_cubes * green_cubes * blue_cubes; }
};
struct TokenGen {
  std::string_view sv;
  std::string_view del;
  operator bool() const { return !sv.empty(); }
  std::string_view operator()() {
    auto r = sv;
    const auto it = sv.find_first_of(del);
    if (it == std::string_view::npos) {
      sv = {};
    } else {
      r.remove_suffix(r.size() - it);
      sv.remove_prefix(it + 1);
    }
    return r;
  }
};

Game parseGameData(const std::string_view data) {
  auto gameNrStart = data.find(" ");
  auto gameNrEnd = data.find(":");
  auto gameNr = data.substr(gameNrStart + 1, gameNrEnd - gameNrStart - 1);

  auto cubes = data.substr(gameNrEnd + 2);

  auto next_elem = std::find_if(cubes.begin(), cubes.end(),
                                [](char c) { return c == ',' or c == ';'; });
  TokenGen tg{cubes, ",;"};
  std::vector<std::string> items;
  while (tg) {
    std::string trimmed = std::string(tg());
    trimmed.erase(0, trimmed.find_first_not_of(" "));
    trimmed.erase(trimmed.find_last_not_of(" ") + 1);
    items.emplace_back(trimmed);
  }

  Game game;
  std::from_chars(gameNr.begin(), gameNr.end(), game.id);
  for (auto it : items) {
    auto space = it.find(' ');
    auto count = 0;
    std::from_chars(it.c_str(), it.c_str() + space, count);
    auto type = it.substr(space + 1, it.size() - 1);
    game.update_cubes(type, count);
  }
  return game;
};

int main() {
  auto inputVec = helpers::readFileToVec("../inputs/2.txt");
  std::vector<Game> games{};
  for (const auto &line : inputVec) {
    games.emplace_back(parseGameData(line));
  }
  int total = 0;
  long powerTotal = 0;
  for (auto &game : games) {
    total += game.isGamePossible(12, 13, 14);
    powerTotal += game.powerCubes();
  }
  std::cout << "Part1: " << total << "\n";
  std::cout << "Part2: " << powerTotal << "\n";

  return 0;
}
