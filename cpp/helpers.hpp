#include <fstream>
#include <iostream>
#include <string_view>
#include <vector>

namespace helpers {

  std::vector<std::string> readFileToVec(const std::string &path) {
    std::ifstream ss(path.c_str());

    std::vector<std::string> outVec;

    std::string line;
    while (getline(ss, line)) {
      outVec.emplace_back(line);
    }

    return outVec;
  }

  std::string trim(const std::string& s) {
    std::string trimmed{s};

    trimmed.erase(0, trimmed.find_first_not_of(" "));
    trimmed.erase(trimmed.find_last_not_of(" ") + 1);

    return trimmed;
  }
}
