#include "helpers.hpp"
#include <algorithm>
#include <array>
#include <charconv>
#include <iostream>
#include <iterator>
#include <ranges>
#include <regex>
#include <string_view>

constexpr std::string_view digits_regex = "0123456789";
constexpr std::array<std::string_view, 19> digits = {
    "0",   "1",   "2",     "3",    "4",    "5",   "6",     "7",     "8",   "9",
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

int main() {
  auto inputVec = helpers::readFileToVec("../inputs/1.txt");
  auto ans1 = 0;
  for (auto &f : inputVec) {
    auto first = f.at(f.find_first_of(digits_regex)) - '0';
    auto last = f.at(f.find_last_of(digits_regex)) - '0';

    ans1 += (first * 10 + last);
  }
  std::cout << "part1: " << ans1 << "\n";

  auto ans2 = 0;
  for (std::string_view line : inputVec) {
    auto first = -1;
    auto last = -1;
    for (auto pos : std::views::iota(line.begin(), line.end())) {
      auto substr = std::string_view(pos, line.end());
      auto match = std::ranges::find_if(
          digits, [&](auto &&digit) { return substr.starts_with(digit); });
      auto val =
          match != digits.end() ? std::distance(digits.begin(), match) : -1;
      if (val > 9)
        val -= 9;
      if (val != -1) {

        if (first == -1)
          first = val;
        last = val;
      }
    }
    ans2 += (first * 10 + last);
  }

  std::cout << "part2: " << ans2 << "\n";

  return 0;
}
