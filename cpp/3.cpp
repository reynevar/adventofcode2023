#include "helpers.hpp"
#include <cctype>
#include <functional>
#include <numeric>
#include <unordered_set>
#include <utility>

bool isPartNumber(const std::vector<std::string> &inputVec, int row, int start,
                  int end) {
  // Set check range
  auto rangeStart = start;
  auto rangeEnd = end;
  if (start != 0) {
    --rangeStart;
    std::cout << "debug:\t"
              << "change rangeStart to: " << rangeStart << "\n";
  }
  if (end != inputVec[row].size()) {
    ++rangeEnd;
  }

  auto rowRangeStart = row != 0 ? row - 1 : row;
  auto rowRangeEnd = row != inputVec.size() - 1 ? row + 1 : row;

  for (auto i = rowRangeStart; i <= rowRangeEnd; ++i) {
    auto chars = inputVec[i].substr(rangeStart, rangeEnd - rangeStart);

    std::cout << "debug:\t"
              << "checking substr !" << chars << "!\n";
    auto isPartNumber = chars.find_first_not_of("0123456789.");

    if (isPartNumber != std::string::npos) {
      return true;
    }
  }
  return false;
}

std::pair<int, int> nextNumber(const std::string &line, int start) {
  std::pair<int, int> ret{-1, -1};
  auto first = line.find_first_of("0123456789", start);
  if (first != std::string::npos) {
    ret = {first, first};
    auto last = line.find_first_not_of("0123456789", first);
    if (last != std::string::npos) {
      ret = {first, last};
    } else {
      if (std::isdigit(line.back())) {
        ret.second = line.size();
      }
    }
  }
  std::cout << "debug:\t"
            << "Next pair positions: " << ret.first << "\t" << ret.second
            << "\n";
  return ret;
}

static std::pair<int, int> expand(const std::string &line, int start) {
  int s = start, e = start;
  for (; s >= 0 && std::isdigit(line[s]); --s) {
  }
  for (; e < line.size() && std::isdigit(line[e]); ++e) {
  }
  return {s + 1, e};
}

int main() {
  auto inputVec = helpers::readFileToVec("../inputs/3.txt");
  long long sumpart1 = 0;
  for (auto i = 0; i < inputVec.size(); ++i) {
    auto line = inputVec[i];
    if (i > 0) {
      std::cout << "debug:\t"
                << "previous line " << inputVec[i - 1] << "\n";
    }
    std::cout << "debug:\t"
              << "checking line " << line << "\n";
    if (i < inputVec.size() - 1) {
      std::cout << "debug:\t"
                << "next     line " << inputVec[i + 1] << "\n";
    }
    auto pos = 0;
    do {
      auto numberRange = nextNumber(line, pos);
      if (numberRange.first != -1) {
        if (isPartNumber(inputVec, i, numberRange.first, numberRange.second)) {
          std::cout << "debug:\t"
                    << "Found  number"
                    << std::stol(inputVec[i].substr(numberRange.first,
                                                    numberRange.second -
                                                        numberRange.first))
                    << "\n";
          sumpart1 += std::stol(inputVec[i].substr(
              numberRange.first, numberRange.second - numberRange.first));
          std::cout << "debug\t"
                    << "new value: " << sumpart1 << "\n";
          numberRange.first = numberRange.second;
        }
        pos = numberRange.second + 1;
      } else {
        pos = line.size();
      }
    } while (pos < line.size());
  }
  std::cout << "part1: " << sumpart1 << "\n";

  // part 2
  std::vector<std::pair<int, int>> gears;
  for (auto y = 0; y < inputVec.size(); ++y) {
    auto line = inputVec[y];
    for (auto x = 0; x < line.size(); ++x) {
      if (line[x] == '*') {
        std::cout << "Found gear at " << x << "\t" << y << "\n";
        gears.push_back({x, y});
      }
    }
  }
  const std::array<std::array<int, 2>, 8> neighbours = {
      {{-1, -1}, {0, -1}, {1, -1}, {-1, 0}, {1, 0}, {-1, 1}, {0, 1}, {1, 1}}};
  long long sumpart2 = 0;
  for (const auto &gear : gears) {
    std::unordered_set<int> numbers;
    int x = gear.first, y = gear.second;

    for (const auto &delta : neighbours) {
      int dx = delta[0], dy = delta[1];

      if ((0 <= y + dy && y + dy < inputVec.size() && 0 <= x + dx &&
           x + dx < inputVec[y + dy].size()) &&
          std::isdigit(inputVec[y + dy][x + dx])) {

        auto [s, e] = expand(inputVec[y + dy], x + dx);
        auto num = inputVec[y + dy].substr(s, e - s);
        std::cout << "Found number " << num << "\n";
        numbers.insert(std::stoi(num));
      }
    }

    if (numbers.size() == 2) {
      auto product = std::accumulate(numbers.begin(), numbers.end(), 1,
                                     std::multiplies<int>());
      sumpart2 += product;
    }
  }
  std::cout << "part2: " << sumpart2 << "\n";

  return 0;
}
