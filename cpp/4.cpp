
#include "helpers.hpp"
#include <bits/stdc++.h>

int convert(std::string s) { return std::stoi(s); }

std::vector<int> split(std::string s, std::string delim) {
  size_t pos_start = 0, pos_end, delim_len = delim.length();
  std::string token;
  std::vector<int> res;
  s = helpers::trim(s);
  while ((pos_end = s.find(delim, pos_start)) != std::string::npos) {
    token = s.substr(pos_start, pos_end - pos_start);
    if (not token.empty()) {
      res.push_back(convert(token));
    }
    pos_start = pos_end + delim_len;
  }
  res.push_back(convert(s.substr(pos_start)));

  return res;
}

std::pair<long long, long long> solution(std::vector<std::string>& inputVec) {
  long long part1 = 0;
  long long part2 = 0;
  // Tracks number of card copies
  std::vector<int> cards(inputVec.size(), 1);

  for (auto i = 0; i< inputVec.size(); ++i) {
    auto line = inputVec[i];
    int cnt = 0;
    auto pos = line.find(':');
    auto numbers = line.substr(pos + 1);

    auto split_pos = numbers.find('|');
    std::vector<int> winning = split(numbers.substr(0, split_pos), " ");
    std::vector<int> values = split(numbers.substr(split_pos + 1), " ");

    for (auto n : winning) {
      if (std::find(values.begin(), values.end(), n) != values.end()) {
        ++cnt;
      }
    }
    if (cnt != 0) {
      part1 += pow(2, cnt - 1);
      for(auto j = 0; j < cnt; ++j) {
        cards[i+j+1] += cards[i];
      }
    }
    part2 += cards[i];
  }
  return std::pair(part1,part2);
}

int main() {
  auto inputVec = helpers::readFileToVec("../inputs/4.txt");

  auto [ans1,ans2] = solution(inputVec);
  std::cout << "Part1: " << ans1 << "\n";
  std::cout << "Part2: " << ans2 << "\n";

  return 0;
}
